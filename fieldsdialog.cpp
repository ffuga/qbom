#include "fieldsdialog.h"
#include "ui_fieldsdialog.h"

FieldsDialog::FieldsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FieldsDialog)
{
    ui->setupUi(this);

    ui->list->setModel(&m_model);

    connect(ui->buttonInsert, SIGNAL(clicked()), this, SLOT(commandInsert()));
    connect(ui->buttonRemove, SIGNAL(clicked()), this, SLOT(commandDelete()));
}

FieldsDialog::~FieldsDialog()
{
    delete ui;
}


void FieldsDialog::commandInsert()
{
    m_model.insertRow(m_model.rowCount());
}

void FieldsDialog::commandDelete()
{
}
