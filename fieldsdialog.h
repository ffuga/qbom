#ifndef FIELDSDIALOG_H
#define FIELDSDIALOG_H

#include <QDialog>
#include <QStringListModel>

namespace Ui {
    class FieldsDialog;
}

class FieldsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FieldsDialog(QWidget *parent = 0);
    ~FieldsDialog();

    void setFields(const QStringList &list) {
        m_fields = list;
        m_model.setStringList(list);
    }

    QStringList fields() const {
        return m_model.stringList();
    }

private:
    Ui::FieldsDialog *ui;

    QStringList m_fields;
    QStringListModel m_model;

public slots:
    void commandInsert();
    void commandDelete();
};

#endif // FIELDSDIALOG_H
