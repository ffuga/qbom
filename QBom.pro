QT       += core gui sql

TARGET = QBom
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    fieldsdialog.cpp

HEADERS  += mainwindow.h \
    fieldsdialog.h

FORMS    += mainwindow.ui \
    fieldsdialog.ui

OTHER_FILES += \
    Info.plist \
    QBom.icns \
    QBom2.icns

RESOURCES += \
    qbom.qrc


macx: {
    QMAKE_MAC_SDK=/Developer/SDKs/MacOSX10.6.sdk
    ICON = QBom.icns

    QMAKE_INFO_PLIST = Info.plist
    ### qmake 4.7.4 workaround
    QMAKE_INFO_PLIST_OUT = QBom.app/Contents/Info.plist

    missing.target = dummy
    missing.depends = QBom.app/Contents/Info.plist QBom.app/Contents/Resources/QBom.icns

    QMAKE_EXTRA_TARGETS = missing

    QMAKE_PRE_LINK = make dummy
}

