#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QModelIndex>

class QSqlTableModel;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QSqlDatabase m_db;
    QSqlTableModel *m_model;
    bool m_reverting;

    void openDatabase(QString filename);
    void createInfrastructure();
    void bindDatabaseView();
    void openCsvFile (QString filename, QList<QStringList> &data);
    void addFileToMRU(QString filename);
    void loadMRU();

public slots:
    void commandNew();
    void commandOpen();
    void commandOpenFile (QString file);
    void commandSync();
    void commandExport();
    void commandFields();

    void dataChanged (QModelIndex,QModelIndex);
};

#endif // MAINWINDOW_H
