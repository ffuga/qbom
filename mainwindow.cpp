#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileInfo>
#include <QDebug>
#include <QFileDialog>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <stdexcept>
#include <QMessageBox>
#include <QSqlResult>
#include <QSqlRecord>
#include <QSettings>
#include <QSignalMapper>
#include "fieldsdialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), m_db(), m_model(0), m_reverting(false)
{
    ui->setupUi(this);

    connect (ui->action_Nuovo, SIGNAL(triggered()), this, SLOT(commandNew()));
    connect (ui->action_Apri, SIGNAL(triggered()), this, SLOT(commandOpen()));
    connect (ui->action_Importa_Sincronizza, SIGNAL(triggered()), this, SLOT(commandSync()));
    connect (ui->action_Campi_addizionali, SIGNAL(triggered()), this, SLOT(commandFields()));
    connect (ui->action_Esporta, SIGNAL(triggered()), this, SLOT(commandExport()));

    setWindowIcon(QIcon(":/icons/application"));

    loadMRU();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::commandNew()
{
    MainWindow *currentWindow;

    if (m_db.isOpen()) {
        MainWindow *newmain = new MainWindow();
        newmain->show();
        currentWindow = newmain;
    } else {
        currentWindow = this;
    }

    QString f = QFileDialog::getSaveFileName(this, tr("Selezionare un percorso dove salvare il file"), QString(), tr("Bom Files (*.qbd);;Tutti i file (*.*)"));

    if (!f.isEmpty()) {
        currentWindow->openDatabase(f);
        currentWindow->createInfrastructure();
        currentWindow->bindDatabaseView();
        currentWindow->addFileToMRU(f);
    } else {
        if (currentWindow != this)
            delete currentWindow;
    }
}

void MainWindow::commandOpen()
{
    MainWindow *currentWindow;

    if (m_db.isOpen()) {
        MainWindow *newmain = new MainWindow();
        newmain->show();
        currentWindow = newmain;
    } else {
        currentWindow = this;
    }

    QString f = QFileDialog::getOpenFileName(this, tr("Selezionare File da aprire"), QString(), tr("Bom Files (*.qbd);;Tutti i file (*.*)"));

    if (!f.isEmpty()) {
        currentWindow->commandOpenFile(f);
    } else {
        if (currentWindow != this)
            delete currentWindow;
    }
}

void MainWindow::commandOpenFile (QString file)
{
    MainWindow *currentWindow;

    if (m_db.isOpen()) {
        MainWindow *newmain = new MainWindow();
        newmain->show();
        currentWindow = newmain;
    } else {
        currentWindow = this;
    }

    currentWindow->openDatabase(file);
    currentWindow->bindDatabaseView();
    currentWindow->addFileToMRU(file);
}

void MainWindow::commandSync()
{
    bool b;
    QString fn = QFileDialog::getOpenFileName(this, tr("Selezionare file da importare per sincronizzazione"), QString(), "File CSV (*.csv);;Tutti i file (*.*)");

    if (!fn.isEmpty()) {
        enum { Undefined, Yes, No } clearNewDevices = Undefined;

        QList<QStringList> data;
        openCsvFile (fn,data);

        // --- Fare mappatura dei campi!

        const int n_part = 0, n_value = 1, n_device = 2, n_package = 3, n_description = 4;

        // ---

        int n_added = 0, n_updated = 0;

        b = m_db.transaction();
        Q_ASSERT(b);

        for (QList<QStringList>::iterator it = data.begin(); it != data.end(); ++it) {

            QSqlQuery checkQuery(m_db);
            QString queryString = QString("SELECT id,device FROM bom WHERE id='%1';").arg((*it)[n_part]);

            qDebug() << queryString;
            b = checkQuery.exec(queryString);
            Q_ASSERT(b);

            if (checkQuery.next() /*&& checkQuery.value(0).toInt() > 0*/ ) { // trovato: faccio aggiornamento

                if (clearNewDevices == Undefined && checkQuery.value(1).toString() != (*it)[n_device]) {
                    // trovato, ma il "device" è cambiato
                    int r = QMessageBox::information(this,
                                                     tr("Trovata una discrepanza"),
                                                     tr("E' stato cambiato il device per un elemento gia' inserito. Si vuole aggiornare la voce corrispondente?\nSelezionando 'aggiorna' le modifiche precedenti saranno perse."),
                                                     tr("Aggiorna"), tr("Mantieni"),tr("Interrompi"), 0, 2);
                    switch (r) {
                    case 2:
                        QMessageBox::information(this, tr("Sincronizzazione interrotta."), tr("Il database è stato riportato alle condizioni iniziali."));
                        m_db.rollback();
                        return;
                    case 1:
                        clearNewDevices = No;
                        break;
                    case 0:
                        clearNewDevices = Yes;
                        break;
                    }
                }

                QString x;
                if (clearNewDevices == No)
                    x = (*it)[n_device];
                else
                    x = checkQuery.value(1).toString();

                QString updateString = QString(
                        "UPDATE bom SET value='%1', package='%2', description='%3', device='%5' "
                        " WHERE id='%4';"
                        ).arg((*it)[n_value]).arg((*it)[n_package]).arg((*it)[n_description]).arg((*it)[n_part]).arg(x);

                b = checkQuery.exec(updateString);
                if (!b) {
                    QString err = QString(tr("Si è verificato il seguente errore: %1")).arg(checkQuery.lastError().text());
                    m_db.rollback();

                    QMessageBox::critical(this, tr("Importazione interrotta!"), err);
                    return;
                }

                ++n_updated;
            } else {    // non trovato: inserisco
                QString insertString = QString(
                        "INSERT INTO bom (id,value,device,package,description) "
                        " VALUES ('%1','%2','%3','%4','%5'); "
                        ).arg((*it)[n_part]).arg((*it)[n_value]).arg((*it)[n_device]).arg((*it)[n_package]).arg((*it)[n_description]);

                b = checkQuery.exec(insertString);
                if (!b) {
                    QString err = QString(tr("Si è verificato il seguente errore: %1")).arg(checkQuery.lastError().text());
                    m_db.rollback();

                    QMessageBox::critical(this, tr("Importazione interrotta!"), err);
                    return;
                }

                ++n_added;
            }
        }

        m_db.commit();

        m_model->select();
        QMessageBox::information(this, tr("Sincornizzazione completata"),
                                 QString("Sono stati importati %1 nuovi record e %2 sono stati aggiornati, per un totale di %3 record caricati.")
                                 .arg(n_added).arg(n_updated).arg(n_added + n_updated));
    }
}


void MainWindow::openDatabase( QString f )
{
    if (m_db.isOpen()) {
        m_db.close();
    }

    QFileInfo info(f);

    m_db = QSqlDatabase::addDatabase("QSQLITE", info.fileName());
    m_db.setDatabaseName(f);

    QString caption = QString(tr("QBom - %1")).arg(info.fileName());
    if (!m_db.open()) {
        qCritical() << "Non posso aprire: " << m_db.lastError();
        caption += tr(" (errore)");
    }

    setWindowTitle(caption);
}

void MainWindow::createInfrastructure()
{
    QSqlQuery query(m_db);

    query.exec( "CREATE TABLE bom ("
                " id VARCHAR(5) PRIMARY KEY NOT NULL, "
                " value VARCHAR(10), "
                " device VARCHAR(20),"
                " package VARCHAR(20), "
                " description VARCHAR(50),"
                " manu VARCHAR(20),"
                " pnum VARCHAR(20),"
                " alternatives VARCHAR(30)"
                ");" );
}

void MainWindow::bindDatabaseView()
{
    if (m_model) delete m_model;

    m_model = new QSqlTableModel(this, m_db);
    m_model->setTable("bom");
    ui->table->setModel(m_model);
    m_model->select();

    connect (m_model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(dataChanged(QModelIndex,QModelIndex)));
}

void MainWindow::openCsvFile(QString filename, QList<QStringList> &data)
{
    QFile file (filename);
    if (!file.open(QIODevice::ReadOnly)) {
        throw std::runtime_error("file not found");
    }

    QTextStream txt(&file);

    while (!txt.atEnd()) {
        QStringList l;
        QString line = txt.readLine();

        QStringList tok = line.split(QChar(','));
        for (QStringList::iterator it = tok.begin(); it != tok.end(); ++it) {
            QString s = (*it);

            s.remove(QRegExp("^\"")).remove(QRegExp("\"$"));
            l.push_back(s);
        }
        data.push_back(l);
    }

    qDebug() << data;
}

void MainWindow::addFileToMRU(QString filename)
{
    QSettings set;

    QStringList l;
    l = set.value("MRU").toStringList();
    l.removeAll(filename);

    l.push_front(filename);
    while (l.count() > 4) {
        l.pop_back();
    }

    set.setValue("MRU", l);

    loadMRU();
}

void MainWindow::loadMRU()
{
    ui->menuRecenti->clear();

    QSettings set;
    QStringList l;
    l = set.value("MRU").toStringList();

    QSignalMapper *mapper = new QSignalMapper(this);
    connect (mapper, SIGNAL(mapped(QString)), this, SLOT(commandOpenFile(QString)));

    for (QStringList::iterator it = l.begin(); it != l.end(); ++it) {
        QFileInfo info(*it);

        QAction *action = ui->menuRecenti->addAction(info.absoluteFilePath(),mapper, SLOT(map()));
        mapper->setMapping(action, *it);
    }
}

void MainWindow::dataChanged(QModelIndex topleft, QModelIndex bottomright)
{
    if (topleft != bottomright)
        return;

    if (m_reverting) {
        m_reverting = false;
        return;
    }

    qDebug() << "Sto cambiando: " << topleft << " / " << bottomright;

    if (topleft.column() == 0 || topleft.column() == 2) {       // id oppure device - rompe il legame con la BOM
        int r = QMessageBox::warning(this, tr("Effettuare il cambiamento dei valori?"),
                             tr("Il cambiamento di questi dati rompera' il collegamento con la fonte dati originale."),
                             QMessageBox::Yes, QMessageBox::No);
        if (r == QMessageBox::No) {
            m_reverting = true;
            m_model->revert();
        }
    } else {

        if (topleft.column() < 5) {
            QMessageBox::warning(this, tr("Attenzione!"), tr("Il dato sarà sovrascritto in caso di nuova sincronizzazione!"));
        }

        QString dev = m_model->data(m_model->index(topleft.row(), 2)).toString();
        QString pack = m_model->data(m_model->index(topleft.row(), 3)).toString();
        QString val = m_model->data(m_model->index(topleft.row(), 1)).toString();

        QString queryString = QString("SELECT COUNT(id) FROM(SELECT id FROM bom WHERE device='%1' AND value='%2' AND package='%3');")
                              .arg(dev).arg(val).arg(pack);
        QSqlQuery query(m_db);

        bool b = query.exec(queryString);
        Q_ASSERT(b);

        int v;
        if (query.next() && (v = query.value(0).toInt()) > 1) {
            int r = QMessageBox::information(this,
                                             tr("Applicare il cambiamento agli elementi simili?"),
                                             QString(tr("Sono stati trovati %1 elementi simili (per Tipo, Package e Valore). Applicare il cambiamento anche a questi?")).arg(v),
                                             QMessageBox::No, QMessageBox::Yes);

            if (r == QMessageBox::Yes) {
                QString fld = m_model->headerData(topleft.column(), Qt::Horizontal).toString();
                QString nv = m_model->data(topleft).toString();
                QString queryString = QString ("UPDATE bom SET '%1'='%2' WHERE device='%3' AND value='%4' AND package='%5';")
                                      .arg(fld).arg(nv).arg(dev).arg(val).arg(pack);

                m_reverting = true;
                QSqlQuery query(m_db);
                bool b = query.exec(queryString);
                Q_ASSERT(b);
            }
        }
    }

//    m_model->select();
}


void MainWindow::commandExport()
{
    QString fn = QFileDialog::getSaveFileName(this,
                                              tr("Selezionare file per esportazione"),
                                              QString(), tr("File CSV (*.csv);Tutti i file (*.*)"));

    if (!fn.isEmpty()) {
        QFile file(fn);
        if (!file.open(QIODevice::ReadWrite)) {
            QString msg = QString("Impossibile aprire il file in scrittura: %1").arg(fn);

            QMessageBox::warning(this, msg, file.errorString());
            return;
        }

        QTextStream txt(&file);

        QString flds;
        QSqlQuery query(m_db);
        bool b = query.exec(QString("PRAGMA table_info(bom);"));
        if (!b) {
            throw std::runtime_error(query.lastError().text().toAscii().data());
        }
        unsigned int nflds = 0;
        QString s;
        while (query.next()) {
            if (nflds>0)
                s =",";
            else
                s ="";

            s += query.value(1).toString();

            txt << s;
            flds += s;

           ++nflds;
        }
        txt << endl;

        QString queryString = QString("SELECT %1 FROM bom;").arg(flds);

        b = query.exec(queryString);
        if (!b) {
            QMessageBox::warning(this, tr("La query e' fallita"), query.lastError().text());
            file.close();
            file.remove();
            return;
        }

        while (query.next()) {
            for (unsigned int i = 0; i < nflds; ++i) {
                if (i > 0)
                    txt << ",";
                txt << "\"" << query.value(i).toString() << "\"";
            }
            txt << endl;
        }
        file.close();
        QMessageBox::information(this,tr("Esportazione completata"),
                                 QString(tr("L'esportazione del file %1 e' stata completata con successo").arg(fn)));
    }
}

void MainWindow::commandFields()
{
    QSqlQuery query(m_db);
    QStringList fields;

    bool b = query.exec(QString("PRAGMA table_info(bom);"));
    if (!b) {
        qDebug () << ">>> " << query.lastError();
        return;
    }

    while (query.next()) {
        fields << query.value(1).toString();
    }

    FieldsDialog dialog(this);
    dialog.setFields(fields);
    if (dialog.exec() == QDialog::Accepted) {

    }
}
